package com.jeecg.teamrank.main.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.jeecgframework.minidao.pojo.MiniDaoPage;
import org.springframework.stereotype.Service;

import com.jeecg.teamrank.main.dao.JpTeamPersonDao;
import com.jeecg.teamrank.main.entity.JpTeamPerson;
import com.jeecg.teamrank.main.service.JpTeamPersonService;

/**
 * 描述：</b>JpTeamPersonServiceImpl<br>
 * @author:p3.jeecg
 * @since：2016年07月14日 10时10分49秒 星期四 
 * @version:1.0
 */

@Service("jpTeamPersonService")
public class JpTeamPersonServiceImpl implements JpTeamPersonService {
	@Resource
	private JpTeamPersonDao jpTeamPersonDao;

	@Override
	public JpTeamPerson get(String id) {
		return jpTeamPersonDao.get(id);
	}

	@Override
	public int update(JpTeamPerson jpTeamPerson) {
		return jpTeamPersonDao.update(jpTeamPerson);
	}

	@Override
	public void insert(JpTeamPerson jpTeamPerson) {
		jpTeamPersonDao.insert(jpTeamPerson);
		
	}

	@Override
	public MiniDaoPage<JpTeamPerson> getAll(JpTeamPerson jpTeamPerson, int page, int rows) {
		return jpTeamPersonDao.getAll(jpTeamPerson, page, rows);
	}

	@Override
	public void delete(JpTeamPerson jpTeamPerson) {
		jpTeamPersonDao.delete(jpTeamPerson);
		
	}

	@Override
	public List<JpTeamPerson> findByQueryString() {
		// TODO Auto-generated method stub
		return jpTeamPersonDao.findByQueryString();
	}

	@Override
	public JpTeamPerson findByQuery(String id) {
		// TODO Auto-generated method stub
		return jpTeamPersonDao.findByQuery(id);
	}
	

	
}
